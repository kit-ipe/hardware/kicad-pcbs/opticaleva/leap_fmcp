EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title "OpticalEVA LEAP FMCP "
Date "2022-04-26"
Rev "1.0"
Comp "Karlsruhe Institute of Technology (KIT)"
Comment1 "IPE"
Comment2 "Luis Ardila"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole H102
U 1 1 5E42431F
P 1000 7200
F 0 "H102" H 1100 7246 50  0000 L CNN
F 1 "M2.5_Pad" H 1100 7155 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 1000 7200 50  0001 C CNN
F 3 "~" H 1000 7200 50  0001 C CNN
	1    1000 7200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H103
U 1 1 5E424A3B
P 1650 7000
F 0 "H103" H 1750 7046 50  0000 L CNN
F 1 "M2.5" H 1750 6955 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 1650 7000 50  0001 C CNN
F 3 "~" H 1650 7000 50  0001 C CNN
	1    1650 7000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H104
U 1 1 5E424A41
P 1650 7200
F 0 "H104" H 1750 7246 50  0000 L CNN
F 1 "M2.5" H 1750 7155 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 1650 7200 50  0001 C CNN
F 3 "~" H 1650 7200 50  0001 C CNN
	1    1650 7200
	1    0    0    -1  
$EndComp
Text Notes 850  6800 0    98   ~ 20
Mounting Holes
$Comp
L Mechanical:Fiducial FID101
U 1 1 5E7DB8DE
P 2750 7000
F 0 "FID101" H 2835 7046 50  0000 L CNN
F 1 "Fiducial" H 2835 6955 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 2750 7000 50  0001 C CNN
F 3 "~" H 2750 7000 50  0001 C CNN
	1    2750 7000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID103
U 1 1 5E7DBFC6
P 3250 7000
F 0 "FID103" H 3335 7046 50  0000 L CNN
F 1 "Fiducial" H 3335 6955 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 3250 7000 50  0001 C CNN
F 3 "~" H 3250 7000 50  0001 C CNN
	1    3250 7000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID105
U 1 1 5E7DC2D6
P 3750 7000
F 0 "FID105" H 3835 7046 50  0000 L CNN
F 1 "Fiducial" H 3835 6955 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 3750 7000 50  0001 C CNN
F 3 "~" H 3750 7000 50  0001 C CNN
	1    3750 7000
	1    0    0    -1  
$EndComp
Text Notes 3050 6800 0    98   ~ 20
Fiducials
$Comp
L KIT_Graphic:KIT_LOGO G101
U 1 1 5E7E615F
P 9400 6850
F 0 "G101" H 9400 6586 60  0001 C CNN
F 1 "KIT_LOGO" H 9400 7114 60  0001 C CNN
F 2 "KIT_Graphic:KIT_LOGO_MASK_4.1x1.6mm" H 9400 6850 157 0001 C CNN
F 3 "" H 9400 6850 157 0001 C CNN
	1    9400 6850
	1    0    0    -1  
$EndComp
$Comp
L KIT_Graphic:LA_LOGO G102
U 1 1 5E7E8B14
P 10350 6800
F 0 "G102" H 10725 6800 50  0001 L CNN
F 1 "LA_LOGO" H 10350 6540 50  0001 C CNN
F 2 "KIT_Graphic:LA_LOGO_MASK_NoName" H 10370 6470 50  0001 C CNN
F 3 "" H 10350 6800 50  0001 C CNN
	1    10350 6800
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID102
U 1 1 5E7F77BC
P 2750 7200
F 0 "FID102" H 2835 7246 50  0000 L CNN
F 1 "Fiducial" H 2835 7155 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 2750 7200 50  0001 C CNN
F 3 "~" H 2750 7200 50  0001 C CNN
	1    2750 7200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID104
U 1 1 5E7F77C2
P 3250 7200
F 0 "FID104" H 3335 7246 50  0000 L CNN
F 1 "Fiducial" H 3335 7155 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 3250 7200 50  0001 C CNN
F 3 "~" H 3250 7200 50  0001 C CNN
	1    3250 7200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID106
U 1 1 5E7F77C8
P 3750 7200
F 0 "FID106" H 3835 7246 50  0000 L CNN
F 1 "Fiducial" H 3835 7155 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 3750 7200 50  0001 C CNN
F 3 "~" H 3750 7200 50  0001 C CNN
	1    3750 7200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H101
U 1 1 5E423097
P 1000 7000
F 0 "H101" H 1100 7046 50  0000 L CNN
F 1 "M2.5_Pad" H 1100 6955 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 1000 7000 50  0001 C CNN
F 3 "~" H 1000 7000 50  0001 C CNN
	1    1000 7000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Heatsink #HS101
U 1 1 5E7A3C9C
P 5000 7150
F 0 "#HS101" H 5142 7271 50  0001 L CNN
F 1 "Heatsink" H 5142 7180 50  0000 L CNN
F 2 "" H 5012 7150 50  0001 C CNN
F 3 "https://www.qats.com/DataSheet/ATS-X51310D-C1-R0" H 5012 7150 50  0001 C CNN
F 4 "ATS36780-ND" H 5000 7150 50  0001 C CNN "Digi-Key"
F 5 "ATS1935-ND" H 5000 7150 50  0001 C CNN "Digi-Key2"
	1    5000 7150
	1    0    0    -1  
$EndComp
Text Notes 4700 6800 0    98   ~ 20
Heatsink
Text Notes 6000 6800 0    98   ~ 20
TestPoints
Text Label 8150 2000 2    50   ~ 0
M2C_RX_P[0..23]
Text Label 8150 2100 2    50   ~ 0
M2C_RX_N[0..23]
Text Label 8150 1700 2    50   ~ 0
C2M_TX_P[0..23]
Text Label 8150 1800 2    50   ~ 0
C2M_TX_N[0..23]
Wire Bus Line
	7100 1700 8700 1700
Wire Bus Line
	7100 1800 8700 1800
Wire Bus Line
	7100 2000 8700 2000
Wire Bus Line
	7100 2100 8700 2100
Wire Wire Line
	8700 2300 7100 2300
Wire Wire Line
	8700 2400 7100 2400
$Sheet
S 5050 1150 2050 4450
U 619269F2
F0 "FMCP" 50
F1 "sch/fmcp.sch" 50
F2 "DP_M2C_N[0..23]" I R 7100 2100 50 
F3 "DP_M2C_P[0..23]" I R 7100 2000 50 
F4 "DP_C2M_N[0..23]" O R 7100 1800 50 
F5 "DP_C2M_P[0..23]" O R 7100 1700 50 
F6 "SDA" B R 7100 2300 50 
F7 "SCL" O R 7100 2400 50 
F8 "DPCLK_M2C_P0" I R 7100 3350 50 
F9 "DPCLK_M2C_N0" I R 7100 3450 50 
F10 "Reset_L" O R 7100 2600 50 
F11 "Int_L" I R 7100 2700 50 
F12 "FPGA_CLK_C2M_P" O R 7100 5250 50 
F13 "FPGA_CLK_C2M_N" O R 7100 5350 50 
F14 "DPCLK_C2M_P3" O R 7100 4600 50 
F15 "DPCLK_C2M_N3" O R 7100 4700 50 
F16 "DPCLK_M2C_P1" I R 7100 3950 50 
F17 "DPCLK_M2C_N1" I R 7100 4050 50 
F18 "PG_M2C" I R 7100 1250 50 
F19 "Test" O R 7100 2800 50 
F20 "3V3_SNS_P" B L 5050 4750 50 
F21 "3V3_SNS_N" B L 5050 4850 50 
F22 "12V_SNS_P" B L 5050 5050 50 
F23 "12V_SNS_N" B L 5050 5150 50 
F24 "1V8_SNS_P" B L 5050 4450 50 
F25 "1V8_SNS_N" B L 5050 4550 50 
$EndSheet
$Sheet
S 8700 3250 1050 400 
U 63D78862
F0 "CLKS" 50
F1 "sch/clks.sch" 50
F2 "OSC_CLK_200M_P" O L 8700 3350 50 
F3 "OSC_CLK_200M_N" O L 8700 3450 50 
$EndSheet
Wire Wire Line
	8700 3350 7100 3350
Wire Wire Line
	8700 3450 7100 3450
$Sheet
S 8700 1600 1050 1400
U 619BA206
F0 "LEAP" 50
F1 "sch/leap.sch" 50
F2 "TX_P[0..23]" I L 8700 1700 50 
F3 "TX_N[0..23]" I L 8700 1800 50 
F4 "RX_P[0..23]" O L 8700 2000 50 
F5 "RX_N[0..23]" O L 8700 2100 50 
F6 "SDA" B L 8700 2300 50 
F7 "SCL" I L 8700 2400 50 
F8 "Reset_L" I L 8700 2600 50 
F9 "Int_L" O L 8700 2700 50 
F10 "Test" I L 8700 2800 50 
$EndSheet
Wire Wire Line
	8700 2600 7100 2600
Wire Wire Line
	8700 2700 7100 2700
$Comp
L Connector:TestPoint TP101
U 1 1 61B080B6
P 5850 7100
F 0 "TP101" H 5908 7172 50  0001 L CNN
F 1 "TestPoint" H 5908 7127 50  0001 L CNN
F 2 "TestPoint:TestPoint_Loop_D2.50mm_Drill1.0mm" H 6050 7100 50  0001 C CNN
F 3 "~" H 6050 7100 50  0001 C CNN
F 4 " 262-2034" H 5850 7100 50  0001 C CNN "rs#"
	1    5850 7100
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP102
U 1 1 61B0978A
P 6150 7100
F 0 "TP102" H 6208 7172 50  0001 L CNN
F 1 "TestPoint" H 6208 7127 50  0001 L CNN
F 2 "TestPoint:TestPoint_Loop_D2.50mm_Drill1.0mm" H 6350 7100 50  0001 C CNN
F 3 "~" H 6350 7100 50  0001 C CNN
F 4 " 262-2034" H 6150 7100 50  0001 C CNN "rs#"
	1    6150 7100
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP103
U 1 1 61B09A85
P 6450 7100
F 0 "TP103" H 6508 7172 50  0001 L CNN
F 1 "TestPoint" H 6508 7127 50  0001 L CNN
F 2 "TestPoint:TestPoint_Loop_D2.50mm_Drill1.0mm" H 6650 7100 50  0001 C CNN
F 3 "~" H 6650 7100 50  0001 C CNN
F 4 " 262-2034" H 6450 7100 50  0001 C CNN "rs#"
	1    6450 7100
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP104
U 1 1 61B09D12
P 6750 7100
F 0 "TP104" H 6808 7172 50  0001 L CNN
F 1 "TestPoint" H 6808 7127 50  0001 L CNN
F 2 "TestPoint:TestPoint_Loop_D2.50mm_Drill1.0mm" H 6950 7100 50  0001 C CNN
F 3 "~" H 6950 7100 50  0001 C CNN
F 4 " 262-2034" H 6750 7100 50  0001 C CNN "rs#"
	1    6750 7100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61B0B291
P 5850 7100
F 0 "#PWR?" H 5850 6850 50  0001 C CNN
F 1 "GND" H 5855 6927 50  0000 C CNN
F 2 "" H 5850 7100 50  0001 C CNN
F 3 "" H 5850 7100 50  0001 C CNN
	1    5850 7100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61B0B544
P 6150 7100
F 0 "#PWR?" H 6150 6850 50  0001 C CNN
F 1 "GND" H 6155 6927 50  0000 C CNN
F 2 "" H 6150 7100 50  0001 C CNN
F 3 "" H 6150 7100 50  0001 C CNN
	1    6150 7100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61B0B80A
P 6450 7100
F 0 "#PWR?" H 6450 6850 50  0001 C CNN
F 1 "GND" H 6455 6927 50  0000 C CNN
F 2 "" H 6450 7100 50  0001 C CNN
F 3 "" H 6450 7100 50  0001 C CNN
	1    6450 7100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61B0BA3E
P 6750 7100
F 0 "#PWR?" H 6750 6850 50  0001 C CNN
F 1 "GND" H 6755 6927 50  0000 C CNN
F 2 "" H 6750 7100 50  0001 C CNN
F 3 "" H 6750 7100 50  0001 C CNN
	1    6750 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 1250 7350 1250
Text Notes 550  1150 0    100  ~ 0
NOTES: \n1) PG_M2C signal is pulled up in VCU118\n2) FPGA_CLK needs capacitors in the mezzanine\n3) DPCLKs have capacitors in the VCU118
NoConn ~ 7350 1250
$Comp
L power:GND #PWR?
U 1 1 61FB9A3E
P 9300 4150
F 0 "#PWR?" H 9300 3900 50  0001 C CNN
F 1 "GND" H 9305 3977 50  0000 C CNN
F 2 "" H 9300 4150 50  0001 C CNN
F 3 "" H 9300 4150 50  0001 C CNN
	1    9300 4150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61FB9D73
P 8900 4250
F 0 "#PWR?" H 8900 4000 50  0001 C CNN
F 1 "GND" H 8905 4077 50  0000 C CNN
F 2 "" H 8900 4250 50  0001 C CNN
F 3 "" H 8900 4250 50  0001 C CNN
	1    8900 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62105A13
P 9300 4800
F 0 "#PWR?" H 9300 4550 50  0001 C CNN
F 1 "GND" H 9305 4627 50  0000 C CNN
F 2 "" H 9300 4800 50  0001 C CNN
F 3 "" H 9300 4800 50  0001 C CNN
	1    9300 4800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62105A19
P 8900 4900
F 0 "#PWR?" H 8900 4650 50  0001 C CNN
F 1 "GND" H 8905 4727 50  0000 C CNN
F 2 "" H 8900 4900 50  0001 C CNN
F 3 "" H 8900 4900 50  0001 C CNN
	1    8900 4900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J106
U 1 1 621069D0
P 10000 5250
F 0 "J106" H 10100 5179 50  0000 L CNN
F 1 "Conn_Coaxial" H 10100 5134 50  0001 L CNN
F 2 "Connector_Coaxial:MMCX_Molex_73415-0961_Horizontal_1.6mm-PCB" H 10000 5250 50  0001 C CNN
F 3 " ~" H 10000 5250 50  0001 C CNN
F 4 "Samtec" H 10000 5250 50  0001 C CNN "manf"
F 5 "MMCX-J-P-H-ST-EM1" H 10000 5250 50  0001 C CNN "manf#"
F 6 " 200-MMCXJPHSTEM1 " H 10000 5250 50  0001 C CNN "mouser#"
F 7 "SAM8837-ND" H 10000 5250 50  0001 C CNN "digikey#"
F 8 "158-7743" H 10000 5250 50  0001 C CNN "rs#"
	1    10000 5250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 621069DC
P 10000 5450
F 0 "#PWR?" H 10000 5200 50  0001 C CNN
F 1 "GND" H 10005 5277 50  0000 C CNN
F 2 "" H 10000 5450 50  0001 C CNN
F 3 "" H 10000 5450 50  0001 C CNN
	1    10000 5450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 621069E2
P 9600 5550
F 0 "#PWR?" H 9600 5300 50  0001 C CNN
F 1 "GND" H 9605 5377 50  0000 C CNN
F 2 "" H 9600 5550 50  0001 C CNN
F 3 "" H 9600 5550 50  0001 C CNN
	1    9600 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 3950 7100 3950
Wire Wire Line
	8700 4050 7100 4050
Wire Wire Line
	9100 4600 7100 4600
Wire Wire Line
	8700 4700 7100 4700
$Comp
L Connector:Conn_Coaxial J102
U 1 1 6223BCF8
P 8900 4700
F 0 "J102" H 9000 4629 50  0000 L CNN
F 1 "Conn_Coaxial" H 9000 4584 50  0001 L CNN
F 2 "Connector_Coaxial:MMCX_Molex_73415-0961_Horizontal_1.6mm-PCB" H 8900 4700 50  0001 C CNN
F 3 " ~" H 8900 4700 50  0001 C CNN
F 4 "Samtec" H 8900 4700 50  0001 C CNN "manf"
F 5 "MMCX-J-P-H-ST-EM1" H 8900 4700 50  0001 C CNN "manf#"
F 6 " 200-MMCXJPHSTEM1 " H 8900 4700 50  0001 C CNN "mouser#"
F 7 "SAM8837-ND" H 8900 4700 50  0001 C CNN "digikey#"
F 8 "158-7743" H 8900 4700 50  0001 C CNN "rs#"
	1    8900 4700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J105
U 1 1 6223C9FA
P 9600 5350
F 0 "J105" H 9700 5279 50  0000 L CNN
F 1 "Conn_Coaxial" H 9700 5234 50  0001 L CNN
F 2 "Connector_Coaxial:MMCX_Molex_73415-0961_Horizontal_1.6mm-PCB" H 9600 5350 50  0001 C CNN
F 3 " ~" H 9600 5350 50  0001 C CNN
F 4 "Samtec" H 9600 5350 50  0001 C CNN "manf"
F 5 "MMCX-J-P-H-ST-EM1" H 9600 5350 50  0001 C CNN "manf#"
F 6 " 200-MMCXJPHSTEM1 " H 9600 5350 50  0001 C CNN "mouser#"
F 7 "SAM8837-ND" H 9600 5350 50  0001 C CNN "digikey#"
F 8 "158-7743" H 9600 5350 50  0001 C CNN "rs#"
	1    9600 5350
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J103
U 1 1 6223CD8C
P 9300 3950
F 0 "J103" H 9400 3879 50  0000 L CNN
F 1 "Conn_Coaxial" H 9400 3834 50  0001 L CNN
F 2 "Connector_Coaxial:MMCX_Molex_73415-0961_Horizontal_1.6mm-PCB" H 9300 3950 50  0001 C CNN
F 3 " ~" H 9300 3950 50  0001 C CNN
F 4 "Samtec" H 9300 3950 50  0001 C CNN "manf"
F 5 "MMCX-J-P-H-ST-EM1" H 9300 3950 50  0001 C CNN "manf#"
F 6 " 200-MMCXJPHSTEM1 " H 9300 3950 50  0001 C CNN "mouser#"
F 7 "SAM8837-ND" H 9300 3950 50  0001 C CNN "digikey#"
F 8 "158-7743" H 9300 3950 50  0001 C CNN "rs#"
	1    9300 3950
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J104
U 1 1 6223DB94
P 9300 4600
F 0 "J104" H 9400 4529 50  0000 L CNN
F 1 "Conn_Coaxial" H 9400 4484 50  0001 L CNN
F 2 "Connector_Coaxial:MMCX_Molex_73415-0961_Horizontal_1.6mm-PCB" H 9300 4600 50  0001 C CNN
F 3 " ~" H 9300 4600 50  0001 C CNN
F 4 "Samtec" H 9300 4600 50  0001 C CNN "manf"
F 5 "MMCX-J-P-H-ST-EM1" H 9300 4600 50  0001 C CNN "manf#"
F 6 " 200-MMCXJPHSTEM1 " H 9300 4600 50  0001 C CNN "mouser#"
F 7 "SAM8837-ND" H 9300 4600 50  0001 C CNN "digikey#"
F 8 "158-7743" H 9300 4600 50  0001 C CNN "rs#"
	1    9300 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 5250 8050 5250
Wire Wire Line
	7100 5350 7450 5350
$Comp
L Device:C C?
U 1 1 6227E31A
P 7600 5350
AR Path="/619BA206/6227E31A" Ref="C?"  Part="1" 
AR Path="/6227E31A" Ref="C101"  Part="1" 
F 0 "C101" V 7550 5450 50  0000 L CNN
F 1 "0.1uF" V 7550 5250 50  0000 R CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 7638 5200 50  0001 C CNN
F 3 "~" H 7600 5350 50  0001 C CNN
F 4 "185-1596" H 7600 5350 50  0001 C CNN "rs#"
F 5 "0201, 25V, X5R, 10%" V 7440 5350 50  0001 C CNN "info"
F 6 "Luis" H 7600 5350 50  0001 C CNN "stock"
	1    7600 5350
	0    1    -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 6227E324
P 8200 5250
AR Path="/619BA206/6227E324" Ref="C?"  Part="1" 
AR Path="/6227E324" Ref="C102"  Part="1" 
F 0 "C102" V 8150 5350 50  0000 L CNN
F 1 "0.1uF" V 8150 5150 50  0000 R CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 8238 5100 50  0001 C CNN
F 3 "~" H 8200 5250 50  0001 C CNN
F 4 "185-1596" H 8200 5250 50  0001 C CNN "rs#"
F 5 "0201, 25V, X5R, 10%" V 8040 5250 50  0001 C CNN "info"
F 6 "Luis" H 8200 5250 50  0001 C CNN "stock"
	1    8200 5250
	0    1    -1   0   
$EndComp
Wire Wire Line
	9800 5250 8350 5250
Wire Wire Line
	9400 5350 7750 5350
Text Label 8550 5250 0    50   ~ 0
FPGA_CLK_C2M_C_P
Text Label 8550 5350 0    50   ~ 0
FPGA_CLK_C2M_C_N
Wire Wire Line
	7100 2800 8700 2800
$Comp
L Connector:Conn_Coaxial J101
U 1 1 6223D7A2
P 8900 4050
F 0 "J101" H 9000 3979 50  0000 L CNN
F 1 "Conn_Coaxial" H 9000 3934 50  0001 L CNN
F 2 "Connector_Coaxial:MMCX_Molex_73415-0961_Horizontal_1.6mm-PCB" H 8900 4050 50  0001 C CNN
F 3 " ~" H 8900 4050 50  0001 C CNN
F 4 "Samtec" H 8900 4050 50  0001 C CNN "manf"
F 5 "MMCX-J-P-H-ST-EM1" H 8900 4050 50  0001 C CNN "manf#"
F 6 " 200-MMCXJPHSTEM1 " H 8900 4050 50  0001 C CNN "mouser#"
F 7 "SAM8837-ND" H 8900 4050 50  0001 C CNN "digikey#"
F 8 "158-7743" H 8900 4050 50  0001 C CNN "rs#"
	1    8900 4050
	1    0    0    -1  
$EndComp
$Sheet
S 2650 4150 1500 1450
U 62B94938
F0 "SENSE" 50
F1 "sch/sense.sch" 50
F2 "3V3_SNS_P" B R 4150 4750 50 
F3 "3V3_SNS_N" B R 4150 4850 50 
F4 "12V_SNS_P" B R 4150 5050 50 
F5 "12V_SNS_N" B R 4150 5150 50 
F6 "1V8_SNS_P" B R 4150 4450 50 
F7 "1V8_SNS_N" B R 4150 4550 50 
F8 "SDA" B R 4150 5350 50 
F9 "SCL" I R 4150 5450 50 
$EndSheet
Wire Wire Line
	4150 4450 5050 4450
Wire Wire Line
	4150 4550 5050 4550
Wire Wire Line
	4150 4750 5050 4750
Wire Wire Line
	4150 4850 5050 4850
Wire Wire Line
	4150 5050 5050 5050
Wire Wire Line
	4150 5150 5050 5150
Wire Wire Line
	4150 5350 4500 5350
Wire Wire Line
	4150 5450 4500 5450
Text Label 4500 5350 2    50   ~ 0
SDA
Text Label 4500 5450 2    50   ~ 0
SCL
Text Label 7950 2300 2    50   ~ 0
SDA
Text Label 7950 2400 2    50   ~ 0
SCL
$Comp
L KIT_Graphic:Lilith G103
U 1 1 62686396
P 10950 6850
F 0 "G103" H 10950 6599 60  0001 C CNN
F 1 "Lilith" H 10950 7101 60  0001 C CNN
F 2 "KIT_Graphic:Lilith" H 10950 6850 100 0001 C CNN
F 3 "" H 10950 6850 100 0001 C CNN
	1    10950 6850
	1    0    0    -1  
$EndComp
$EndSCHEMATC
